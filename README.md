# Survey and implement new deep learning algorithms on any DL framework

**Author**: Xin Fu, Yi Rong, Runnan Cao, Wenjun Lin, Zihan Chen (Wuhan University)

* [ACGAN](https://github.com/Microsoft/samples-for-ai/tree/master/examples/keras/acgan_tf)
* [Capsule Network](https://github.com/Microsoft/samples-for-ai/tree/master/examples/keras/capsnet_tf)
* [DCGAN](https://github.com/Microsoft/samples-for-ai/tree/master/examples/keras/dcgan_tf)

## ACGAN

Keras implementation of [Conditional Image Synthesis With Auxiliary Classifier GANs](https://arxiv.org/abs/1610.09585)

Train an ACGAN on MNIST dataset. 

Test passed on local Windows machine with VS 2017
ENV: Python 3.6.3, Keras 2.12, Tensorflow 1.4.0

Screenshot:
![snipaste_2018-03-31_17-16-40](https://user-images.githubusercontent.com/5097752/38162049-eb1f87f6-350c-11e8-8b11-3d1284de7311.png)
![plot_epoch_002_generated](https://user-images.githubusercontent.com/5097752/38162054-00243ae8-350d-11e8-8a62-317a74a451ae.png)

## CapsNet

Keras implementation of NIPS 2017 Paper: ["Dynamic Routing Between Capsules"](http://papers.nips.cc/paper/6975-dynamic-routing-between-capsules)
Train a simple Capsule Network on the CIFAR10 small images dataset.

Test passed on local Windows machine with VS 2017
ENV: Python 3.6.3, Keras 2.12, Tensorflow 1.4.0

LICENSE: MIT

## DCGAN

Add a Keras example of  DCGAN (Deep Convolution Generative Adversarial Networks) to generate handwritten digits
Implement the network and the results of it can be visualized and saved as pictures. It's helpful to understand how gan operates.

Project has been tested on local Windows 10 machine with newest Visual Studio 2017 and VS Tools for AI.

License: MIT